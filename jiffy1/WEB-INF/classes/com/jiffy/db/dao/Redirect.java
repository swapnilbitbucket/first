package com.jiffy.db.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jiffy.bean.EmployeeDetails;
import com.jiffy.bean.SaveBean;
import com.jiffy.constant.DBConnection;
import com.jiffy.constant.ReplaceNull;

/**
 * Servlet implementation class Redirect
 */
public class Redirect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Redirect() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doProcess(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		HttpSession session 	= request.getSession();
		String userid 			= ReplaceNull.replace((String)session.getAttribute("userid"));
		DBConnection db 		= new DBConnection();
		Connection conn 		= null;
		PreparedStatement pstmt	= null;
		ResultSet rs 			= null;
		if(userid.length()>0){
			try{
				conn = db.getDBConnection(getServletContext());
				boolean isValidUser = false;
				String query =  " select user_name,user_type,password,emailid,um.user_id,doj"+
								" from USER_MASTER um  "+
								" inner join USER_DETAIL ud on um.user_id=ud.user_id "+
								" where um.user_id=? ";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, userid);
				rs = pstmt.executeQuery();
				if(rs.next()){
					isValidUser = true;
					EmployeeDetails employee = new EmployeeDetails();
					SaveBean saveBean = new SaveBean();
					employee.setPassword(ReplaceNull.replace(rs.getString("password")));
					employee.setEmailId(ReplaceNull.replace(rs.getString("emailid")));
					employee.setUserName(ReplaceNull.replace(rs.getString("user_name")));
					employee.setUserType(ReplaceNull.replace(rs.getString("user_type")));
					employee.setUserId(ReplaceNull.replace(rs.getString("user_id")));
					employee.setDOJ(rs.getDate("DOJ"));
					
					session.setAttribute("employee",employee);
				}rs.close();pstmt.close();
				
				if(isValidUser){
					response.sendRedirect("home");
					return;
				}else{
					session.setAttribute("msg", "Invalid User ID or Password");
					response.sendRedirect("login");
					return;
				}				
			}catch(Exception e){
				System.out.println("Exception in Redirect.java :: "+e);
				//e.printStackTrace();
			}finally{
				db.closeResultSet(rs);
				db.closePreparedStatement(pstmt);
				db.closeConnection(conn);
				db=null;
			}
		}else{
			session.setAttribute("msg", "Invalid User ID or Password.");
			response.sendRedirect("login");
			return;
		}
	}
}
