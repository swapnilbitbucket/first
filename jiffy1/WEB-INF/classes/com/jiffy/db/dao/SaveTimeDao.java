package com.jiffy.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.jiffy.bean.SaveBean;

public class SaveTimeDao {

	public boolean saveTimeInDB(Connection con,PreparedStatement pstmt, ResultSet rs,SaveBean saveBean){
		try{
			boolean isPresent=false;
			String query = " select * from USER_TIMESHEET where user_id=? and day=? and month=? and year=? ";
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, saveBean.getUserId());
			pstmt.setString(2, saveBean.getDay());
			pstmt.setString(3, saveBean.getMonth());
			pstmt.setString(4, saveBean.getYear());
			rs = pstmt.executeQuery();
			if(rs.next()){
				isPresent=true;
			}rs.close();pstmt.close();
			
			if(isPresent){
				query = " update USER_TIMESHEET set start_hour=?, start_minute=?, end_hour=?,end_minute=? "+
						" where user_id=? and day=? and month=? and year=? ";
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, saveBean.getStartHour());
				pstmt.setString(2, saveBean.getStartMinute());
				pstmt.setString(3, saveBean.getEndHour());
				pstmt.setString(4, saveBean.getEndMinute());
				pstmt.setString(5, saveBean.getUserId());
				pstmt.setString(6, saveBean.getDay());
				pstmt.setString(7, saveBean.getMonth());
				pstmt.setString(8, saveBean.getYear());
				
				int i = pstmt.executeUpdate();
				pstmt.close();
				if(i>0){
					return true;
				}
			}else{
				query = " insert into USER_TIMESHEET(user_id,day,month,year,start_hour,start_minute,end_hour,end_minute) values(?,?,?,?,?,?,?,?) ";
				
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, saveBean.getUserId());
				pstmt.setString(2, saveBean.getDay());
				pstmt.setString(3, saveBean.getMonth());
				pstmt.setString(4, saveBean.getYear());
				pstmt.setString(5, saveBean.getStartHour());
				pstmt.setString(6, saveBean.getStartMinute());
				pstmt.setString(7, saveBean.getEndHour());
				pstmt.setString(8, saveBean.getEndMinute());
				
				int i = pstmt.executeUpdate();
				pstmt.close();
				if(i>0){
					return true;
				}
			}
			
		}catch(Exception e){
			System.out.println("Exception in SaveTimeDao.java "+e);
			//e.printStackTrace();
		}
		
		return false;
	}
}
