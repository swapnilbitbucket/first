package com.jiffy.util;

public class Constant {

	public static final String EMPLOYEE	= "E";
	public static final String ADMIN 	= "A";
	
	//days Constant used in DateTime
	
	public static final int JANUARY 	= 0;
	public static final int FEBRUARY 	= 1;
	public static final int MARCH 		= 2;
	public static final int APRIL 		= 3;
	public static final int MAY 		= 4;
	public static final int JUNE 		= 5;
	public static final int JULY 		= 6;
	public static final int AUGUST 		= 7;
	public static final int SEPTEMBER 	= 8;
	public static final int OCTOBER 	= 9;
	public static final int NOVEMBER 	= 10;
	public static final int DECEMBER 	= 11;
	
	public static final String SATURDAY = "SATURDAY";
	public static final String SUNDAY 	= "SUNDAY";
	
	
}
