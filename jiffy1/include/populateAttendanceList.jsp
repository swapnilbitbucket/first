<%@page import="java.util.HashMap"%>
<%@page import="com.jiffy.db.dao.UserTimeInfo"%>
<%@page import="com.jiffy.bean.SaveBean"%>
<%@page import="com.jiffy.util.Constant,com.jiffy.util.DateTime,com.jiffy.constant.ReplaceNull"%>
<%
try{
	String flag  	= ReplaceNull.replace(request.getParameter("flag"));
	String userId  	= ReplaceNull.replace(request.getParameter("userId"));
	
	if(flag.length()>0){
		int finalMonth 	= Integer.parseInt(ReplaceNull.replace(request.getParameter("month")));
		int finalYear  	= Integer.parseInt(ReplaceNull.replace(request.getParameter("year")));
		int daysInMonth = DateTime.getNumberOfDaysInMonth(finalMonth, finalYear);
		
		UserTimeInfo userInfo = new UserTimeInfo();
		HashMap<String,HashMap<Integer,SaveBean>> saveBeanMap = new HashMap<String,HashMap<Integer,SaveBean>>();
		
		saveBeanMap = userInfo.getUserTimeInfo(getServletContext(), userId, finalMonth, finalYear);
		
		HashMap<Integer,SaveBean> tempSaveBean = new HashMap<Integer,SaveBean>();
		
		tempSaveBean = saveBeanMap.get(userId);
		
		String dayName = "";
		%>
		<table width="60%" align="center" border="1">
			<tr>
				<td>Date</td>
				<td>Day</td>
				<td>Start Time</td>
				<td>End Time</td>
				<td>Task</td>
			</tr>
			<%for(int i=1;i<=daysInMonth;i++){
				SaveBean saveBean=null;
				boolean isPresnet=false;
				if(tempSaveBean.containsKey(i)){
					saveBean = tempSaveBean.get(i);
					isPresnet=true;
				}
				
				dayName = DateTime.getDayName(i,finalMonth+1,finalYear); %>
				<tr>
					<td><%=i%> <%= DateTime.getMonthName(finalMonth)%> <%=finalYear %></td>
					<td><%= dayName %></td>
					<%if(dayName.equalsIgnoreCase(Constant.SATURDAY) || dayName.equalsIgnoreCase(Constant.SUNDAY)){%>
						<td colspan="3" align="center">Week Off</td>
					<%}else{%>
						<td>
							<%if(isPresnet && ReplaceNull.replace(saveBean.getStartHour()).length()>0){%>
								<center><%= saveBean.getStartHour()  %>:<%= saveBean.getStartMinute() %> AM</center>
								<input type="hidden" value="<%=saveBean.getStartHour()%>" name="cmbStartHour<%=i%>" id="cmbStartHour<%=i%>" >
								<input type="hidden" value="<%=saveBean.getStartMinute()%>" name="cmbStartMinute<%=i%>" id="cmbStartMinute<%=i%>" >
								
							<%}else{%>
								<select name="cmbStartHour<%=i%>" id="cmbStartHour<%=i%>" onchange="fncShowEndTime(<%=i%>);">
									<option value="">Hour</option>
									<%for(int j=1;j<=24;j++){%>
										<option value="<%=j%>"><%=j%></option>
									<%}%>
								</select>
								<select name="cmbStartMinute<%=i%>" id="cmbStartMinute<%=i%>" onchange="fncShowEndTime(<%=i%>);">
									<option value="">Minute</option>
									<%for(int j=1;j<=60;j++){%>
										<option value="<%=j%>"><%=j%></option>
									<%}%>
								</select>
								<input type="button" value="ok" name="btnStartOk<%=i%>" id="btnStartOk<%=i%>" onclick="fncSaveTime('<%=i%>','<%=finalMonth%>','<%=finalYear%>');" disabled>
							<%}%>
						</td>
						<td>
							<%if(isPresnet && ReplaceNull.replace(saveBean.getEndHour()).length()>0){%>
								<center><%= saveBean.getEndHour()  %>:<%= saveBean.getEndMinute() %> PM</center>
							<%}else{%>
								<select name="cmbEndHour<%=i%>" id="cmbEndHour<%=i%>" <%=isPresnet ? "" : "disabled" %>>
									<option value="">Hour</option>
									<%for(int j=1;j<=24;j++){%>
										<option value="<%=j%>"><%=j%></option>
									<%}%>
								</select>
								<select name="cmbEndMinute<%=i%>" id="cmbEndMinute<%=i%>" <%=isPresnet ? "" : "disabled" %>>
									<option value="">Minute</option>
									<%for(int j=1;j<=60;j++){%>
										<option value="<%=j%>"><%=j%></option>
									<%}%>
								</select>
								<input type="button" value="ok" name="btnEndOk<%=i%>" id="btnEndOk<%=i%>" onclick="fncSaveTime('<%=i%>','<%=finalMonth%>','<%=finalYear%>');" <%=isPresnet ? "" : "disabled" %>>
							<%}%>
						</td>
						<td><input type="submit" onclick="fncSubmit();"></td>
					<%}%>
				</tr>
			<%}%>
		</table>
		<%
	}
}catch(Exception e){
	System.out.println("Exception in populateAttendanceList.jsp :: "+e);
	e.printStackTrace();
}
%>